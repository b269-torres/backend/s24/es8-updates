//console.log('Hello world');


const getCube = 2**3;

console.log(`The cube of 2 is ${getCube}`);

const address = ["Sampaloc", "Manila"];
//destructuring the array

const [firstAddress, secondAddress] = address

//print using template literals
console.log(`I live at ${firstAddress}, ${secondAddress}.`);

//Array of numbers
const arrayNumbers = [1,2,3,4,5,6,7];
arrayNumbers.forEach((arrayNumbers) =>{
	console.log(`${arrayNumbers}`)
});

//reducedNumbers 
const indexValueofArray = 0
const sumAllTheArray = arrayNumbers.reduce((total, next)=> {return total + next});
console.log(sumAllTheArray);


//Animal
const animal = {
	kindOfAnimal: "Dog",
	animalName: "Browney",
	animalAge: "10",
	animalType: "Doberman"
}

const {kindOfAnimal, animalName, animalAge, animalType} = animal;
console.log(animal);

